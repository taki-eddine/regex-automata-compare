## @brief Gives the absolute path of a header file
function(absolute_header_path header_file return)
   execute_process(COMMAND           find /usr/include/ /usr/local/include/ ${CMAKE_SOURCE_DIR}/include/ -path "*/${header_file}" -print0
                   WORKING_DIRECTORY .
                   RESULT_VARIABLE   result
                   OUTPUT_VARIABLE   output)
   set(${return} ${output} PARENT_SCOPE)
endfunction()

## @brief Gives the absolute path of a header file only inside the include directory of the project
function(absolute_header_path_proj header_file return)
   execute_process(COMMAND           find ${CMAKE_SOURCE_DIR}/include/ -path "*/${header_file}" -print0
                   WORKING_DIRECTORY .
                   RESULT_VARIABLE   result
                   OUTPUT_VARIABLE   output)
   set(${return} ${output} PARENT_SCOPE)
endfunction()

#[[
 * @brief Create a include file wich list the header of specific directory.
 * @param relative_path  The include directory path.
 * @param directory_name Relative path of the directory wich contains the headers files.
 * @return void.
]]
function(header_directory directory_name)
   # get list of files in include folder.
   file(GLOB includes_files
        RELATIVE "${CMAKE_SOURCE_DIR}/include"
        "${CMAKE_SOURCE_DIR}/include/${directory_name}/*.hpp")

   # convert the list to files into #include.
   foreach(include_file ${includes_files})
      set(include_statements "${include_statements}\n#include \"${include_file}\"")
   endforeach()

   # fill the template.
   configure_file("${CMAKE_SOURCE_DIR}/cmake/templates/directory_header.hpp.in"
                  "${CMAKE_SOURCE_DIR}/include/${directory_name}.hpp")
endfunction()

#[[
 * @brief Create a include file for each directory in include paths.
]]
function(header_directories)
   # get headers list and extract the dirname for each one.
   file(GLOB_RECURSE header_list
        RELATIVE "${CMAKE_SOURCE_DIR}/include"
        "${CMAKE_SOURCE_DIR}/include/*")

   foreach(header_path ${header_list})
      get_filename_component(dir_path ${header_path} DIRECTORY)
      # exclude internal directory.
      if(NOT "${header_path}" MATCHES "(.*)internal/(.*)")
         set(dir_list ${dir_list} ${dir_path})
      endif()
   endforeach()
   list(REMOVE_DUPLICATES dir_list)

   # foreach directory create a header.
   foreach(dir_path ${dir_list})
      header_directory(${dir_path})
   endforeach()
endfunction()

#[[
 * @brief Generate a header file wich merge divided header
]]
function(header_merge)
   file(GLOB_RECURSE header_list RELATIVE "${CMAKE_SOURCE_DIR}/include/" "${CMAKE_SOURCE_DIR}/include/*.hxx")

   # exclude namespace directories.
   foreach(header ${header_list})
      get_filename_component(dir_path ${header} DIRECTORY)
      if("${dir_path}" MATCHES "(.*)\\.nm")
         list(REMOVE_ITEM header_list "${header}")
      endif()
   endforeach()

   foreach(header_file ${header_list})
      set(separated_file "${header_file}")

      string(REPLACE ".hxx" "" name_without_ext ${header_file})

      set(inline_header "${name_without_ext}.ixx")
      if(EXISTS "${CMAKE_SOURCE_DIR}/include/${inline_header}")
         set(separated_file ${separated_file} ${inline_header})
      endif()

      set(template_header "${name_without_ext}.txx")
      if(EXISTS "${CMAKE_SOURCE_DIR}/include/${template_header}")
         set(separated_file ${separated_file} ${template_header})
      endif()

      set(implement_header "${name_without_ext}.imp")
      if(EXISTS "${CMAKE_SOURCE_DIR}/include/${implement_header}")
         set(separated_file ${separated_file} ${implement_header})
      endif()

      string(REPLACE "internal/" "" gen_header ${name_without_ext})
      set(gen_header "${gen_header}.hpp")

      # convert the list to files into #include.
      foreach(include_file ${separated_file})
         set(include_statements "${include_statements}\n#include \"${include_file}\"")
      endforeach()

      # fill the template.
      configure_file("${CMAKE_SOURCE_DIR}/cmake/templates/directory_header.hpp.in"
                     "${CMAKE_SOURCE_DIR}/include/${gen_header}")

      unset(separated_file)
      unset(inline_header)
      unset(template_header)
      unset(include_statements)
   endforeach()
endfunction()

#[[
 *
]]
function(directory_namespace)
   # search for namespace directories.
   file(GLOB_RECURSE header_list RELATIVE "${CMAKE_SOURCE_DIR}/include/" "${CMAKE_SOURCE_DIR}/include/*.hxx")

   # collect namespace directories.
   foreach(header ${header_list})
      get_filename_component(dir_path ${header} DIRECTORY)
      if("${dir_path}" MATCHES "(.*)\\.nm(.*)")
         list(APPEND dir_list ${dir_path})
      endif()
   endforeach()
   list(REMOVE_DUPLICATES dir_list)

   foreach(dir_name ${dir_list})
      file(GLOB header_list RELATIVE "${CMAKE_SOURCE_DIR}/include/" "${CMAKE_SOURCE_DIR}/include/${dir_name}/*")
      list(SORT header_list)

      # convert the list to files into #include.
      string(REGEX REPLACE "internal/|\\.nm" "" gen_header ${dir_name})
      set(gen_header "${gen_header}.hpp")
      foreach(include_file ${header_list})
         set(include_statements "${include_statements}\n#include \"${include_file}\"")
      endforeach()

      # fill the template.
      configure_file("${CMAKE_SOURCE_DIR}/cmake/templates/directory_header.hpp.in"
                     "${CMAKE_SOURCE_DIR}/include/${gen_header}")

      unset(header_list)
      unset(include_statements)
   endforeach()
endfunction()
