////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <string>
#include <vector>
#include <list>
#include <memory>
#include "regex_automata_compare/StateType.hpp"
#include "regex_automata_compare/Transition.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

/// \brief Class State.
class State {
    friend class Automata;

  private:
    std::string _label = "";
    StateType   _type  = StateType::NORMAL;

    std::vector< std::shared_ptr<Transition> > _edges_out = { };
    std::vector< std::shared_ptr<Transition> > _edges_in  = { };

    std::list< std::shared_ptr<State> > _composers = { };  /// < the states wich is composed by.

    ///
    /// \return
    const std::vector< std::shared_ptr<Transition> >&
    _getEdgesOut() const;

    ///
    /// \return
    const std::vector< std::shared_ptr<Transition> >&
    _getEdgesIn() const;

    /// \brief Add a EdgeOut to this state.
    /// \param transition
    void _addEdgeOut(const std::shared_ptr<Transition>& transition);

    /// \brief Remove EdgeOut.
    /// \param transition
    void _removeEdgeOut(const Transition& transition);

    /// \brief Add EdgeIn.
    /// \param transition
    void _addEdgeIn(const std::shared_ptr<Transition>& transition);

    /// \brief Removes a EdgeIn transtion from the list.
    /// \param transition
    void _removeEdgeIn(const Transition& transition);

    /// \brief Add new composer to this state.
    /// \param state state to add it.
    ///
    /// \notice must be `std::shared_ptr' to share the pointer
    ///         in case it's new for this states
    bool _addCompose(const std::shared_ptr<State>& state, bool regenerate_label = true);

    ///
    /// \param states
    void _addComposers(const std::list< std::shared_ptr<State> >& states);

    ///
    void _removeCompose(const State& state);

    ///
    bool _hasComposer(const std::shared_ptr<State>& state);

    /// \brief Generate a label from the composers list.
    ///        The generated name is concatenation of composers labels.
    void _generateLabelFromComposers();

  public:
    ///
    State();

    ///
    /// \param name
    /// \param type
    State(const std::string& name, const StateType type = StateType::NORMAL);

    ///
    /// \param other
    /// \return
    bool operator==(const State& other) const;

    ///
    /// \return
    const std::string& getLabel() const;

    ///
    /// \param name
    void setName(const std::string& name);

    ///
    /// \return
    StateType getType() const;

    /// \brief toString.
    /// \return String.
    std::string
    toString() const;
} ;

} // namepace regex_automata_compare
