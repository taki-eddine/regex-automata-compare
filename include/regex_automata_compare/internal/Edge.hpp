////////////////////////////////////////////////////////////////////////////////
#pragma once
//#include "regex_automata_compare/internal/StatePack.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {
namespace priv {

// * Forward Declaration ~~~~~~~~~~~~~//
class StatePack;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

class Edge {
  private:
    StatePack& _from;
    StatePack& _to;
    char       _alpha = '\0';

  public:
    ///
    Edge(StatePack& from, char alpha, StatePack& to);

    const StatePack& getFromState() const;
    const StatePack& getToState() const;
    char getAlpha() const;
} ;

} // namepace priv
} // namepace regex_automata_compare
