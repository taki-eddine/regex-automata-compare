////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <vector>
#include <memory>
#include "regex_automata_compare/Automata.hpp"
#include "regex_automata_compare/internal/StatePack.hpp"
#include "regex_automata_compare/internal/Edge.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {
namespace priv {

class NFA {
  private:
    Automata& _automata;  /// < the automata to convert it from NFA to DFA.

    std::vector< std::unique_ptr<StatePack> > _states; /// < states of this NFA.
    std::vector< std::unique_ptr<Edge> >      _edges;  /// < the edges of this NFA.

  public:
    /// Constructor.
    /// \param automata Automata to work on it.
    NFA(Automata& automata);

    ///
    void addStateToStatePack(State& state, StatePack& state_pack);

    ///
    void addEdge(Edge& edge);
} ;

} // namespace priv
} // namespace regex_automata_compare
