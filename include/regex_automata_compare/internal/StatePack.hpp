////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <vector>
#include <memory>
#include "regex_automata_compare/State.hpp"
#include "regex_automata_compare/internal/Edge.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {
namespace priv {

class StatePack {
  private:
    std::vector< std::unique_ptr<State> > _states{ };
    std::vector< Edge* > _edges_out{ };
    std::vector< Edge* > _edges_in{ };

    ///
    /// \param edge_in
    void _addEdgeIn(Edge& edge_in);

    ///
    /// \param edge_out
    void _addEdgeOut(Edge& edge_out);

  public:
    /// Compare to another StatePack.
    /// \param pack State Pack.
    /// \return boolean.
    bool operator ==(StatePack& pack);

    ///
    /// \param state
    void addState(State& state);
} ;

} // namespace priv
} // namespace regex_automata_compare::priv
