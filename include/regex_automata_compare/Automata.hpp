////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <fstream>
#include <memory>
#include <functional>
#include <vector>
#include <set>
#include <list>
#include <forward_list>
#include "regex_automata_compare/State.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

//! \brief class Automata.
class Automata {
  private:
    std::list< std::shared_ptr<State> >        _states      = { };
    std::vector< std::shared_ptr<Transition> > _transitions = { };

    // TODO
    // -+ `_alphabetes' must contains only alpha char,
    // -+ will use this format in `_isStateComplete'.
    std::set< std::pair<char, bool> > _alphabets  { };

    std::list< std::shared_ptr<State> > _init_states = { };

    bool _is_deterministic = false;  // < is this automata DFA.

    ///
    /// \param label
    /// \return
    void _removeStateFromInitList(State& state);

    /// \brief This used only if we don't know about the given object, so it will
    ///        compare each element to find the pointer in the list.
    /// \param label label to compare with.
    /// \return pointer or nullptr if not found.
    std::shared_ptr<State> _findState(const std::string& label);

    /// \brief This use if we are sure about the existance of the object,
    ///        this will compare the address of ojbect with the pointer in list.
    /// \param state State object to compare with.
    /// \return iterator.
    auto _findState(const State& state);

    /// \brief Find the pointer of the transition.
    ///        Use this only if the given transition is not sure of it's exitance.
    /// \param start the start state (must be exist in the automata).
    /// \param alpha the alpha transition.
    /// \param reach the reach state (must be exist in the automata).
    /// \return pointer (std::shared_ptr).
    ///
    /// \notice For both the states, use `getState()'.
    std::shared_ptr<Transition> _findTransition(State& start, char alpha, State& reach);

    /// \brief Find the pointer of the transition.
    ///        Use only if the automata is DFA, because if it's NFA could be
    ///        exist multiple Transition for the same alpha.
    /// \param start the start state (must be exist in the automata).
    /// \param alpha the alpha transition.
    /// \return pointer (std::shared_ptr).
    ///
    /// \notice `start` must be exist, use `getState()'.
    std::shared_ptr<Transition> _findTransition(State& start, char alpha);

    /// \brief Find the pointer of the transition, this method considere
    ///        the given object already exist, will compare directly between pointers.
    /// \param transition Transtion object.
    /// \return iterator.
    auto _findTransition(const Transition& transition);

    //!
    void _resetAlphabetsStates();

    ///
    /// \param c
    /// \return
    std::pair<char, bool>&
    _findAlphabet(const char c);

    ///
    /// \param state
    /// \return
    bool _isStateComplete(State& state);

    ///
    /// \param state
    /// \return
    bool _isStateAmbiguous(State& state);

    ///
    /// \brief Convert the state to a complete one.
    /// \param state The state to transform it.
    void _completeState(State& state, State& trap) = delete;

    /// \brief This method will make any state complete.
    void _completeStates();

    ///
    void _disambiguateStates();

    ///
    void _removeUnreachableStates();

    /// \brief Will remove all in and out transition of the state.
    /// \param state State to remove it's links.
    void _removeStateLinks(State& state);

    /// \brief Search for reachable states with the '&'(Epsilon) alpha.
    /// \param state States pack.
    void _closure(State& state);

    /// \brief Search for reachable states with specific alpha.
    /// \param state States pack.
    /// \param alpha Char.
    /// \return Reachable States.
    std::shared_ptr<State> _goto(State& state, char alpha);

    /// \brief We assign to each state a unique label.
    void _assignLabesToStates();

    //---------------------------------------
    // TODO
    // -+ create method usable only with `minizmize'.
    //---------------------------------------

  public:
    ///
    Automata();

    //!
    bool isDeterministic() const;

    ///
    /// \param label
    /// \param type
    /// \return
    State& addState(const std::string& label, const StateType type = StateType::NORMAL);

    ///
    /// \param state
    /// \return
    State& addState(const std::shared_ptr<State>& p_state);

    ///
    /// \param label
    /// \return
    bool hasState(const std::string& label);

    ///
    /// \param label
    /// \return
    State& findState(const std::string& label);

    //!
    void removeState(State& state);

    //!
    Transition& addTransition(State& from, char alpha, State& to);

    /*!
     * \brief Check if state has transition with alpha.
     * \param state The state to test it.
     * \param alpha The alphabet.
     * \retunr True if the state has one or more transtion with this alpha.
     */
    bool hasTransition(State& state, const char alpha);

    /// \brief Remove transition from the automata.
    /// \param transition transition to remove it.
    void removeTransition(const Transition& transition);

    //!
    void determinize();

    //!
    void minimize();

    //!
    void mergeStates(const std::forward_list<std::reference_wrapper<State>>& states_to_merge);

    /*! \brief Load a automata from file.
     *  \param file_name File path.
     *  \exeption std::ifstream::failer Exception opening file.
     */
    void load(const std::string& filename);

    /*! \brief Save the automara into a file.
     *  \exception std::ifstream::failer Exception opening file.
     */
    void save(const std::string& filename);

    /*! \brief Number of states.
     *  \retunr Size Type.
     */
    std::size_t
    size() const;

    /*! \brief toString.
     *  \return String.
     */
    std::string
    toString() const;
} ;

} // namespace regex_automata_compare
