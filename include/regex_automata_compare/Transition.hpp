////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <string>
#include <memory>
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

// * Forward Declaration ~~~~~~~~~~~~~//
class State;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/// \brief class Transition.
class Transition {
    friend class Automata;

  private:
    std::shared_ptr<State> _start_state = nullptr;
    char                   _alpha       = '\0';
    std::shared_ptr<State> _reach_state = nullptr;

  public:
    //!
    Transition(const std::shared_ptr<State>& start_state, const char alpha, const std::shared_ptr<State>& reach_state);

    //!
    bool operator==(const Transition& other) const;

    //!
    const char& getAlpha() const;

    //!
    const State& getStartState() const;

    //!
    const State& getReachState() const;

    /// \brief toString.
    /// \return String
    std::string
    toString() const;
} ;

} // namepace regex_automata_compare
