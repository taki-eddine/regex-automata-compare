////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

/// @brief enum StateType.
enum class StateType : char {
    INIT   = '+',
    NORMAL = '-',
    FINAL  = '*',
} ;


//!
StateType charToStateType(const char char_state_type);

//!
[[deprecated]]
char stateTypeToChar(const StateType state_type);

} // namepace regex_automata_compare
