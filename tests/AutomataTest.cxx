////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
using namespace testing;
#include "regex_automata_compare/Automata.hpp"
using namespace regex_automata_compare;
////////////////////////////////////////////////////////////////////////////////

//! \brief Automata Test class
class AutomataTest : public Test
{   protected:
      Automata automata = Automata();

      void SetUp() override
      { }

      void TearDown() override
      { }

    public:
      ///
      AutomataTest()
      { }

      ///
      ~AutomataTest()
      { }
};

//--------------------------------------
TEST_F(AutomataTest, add)
{   //automata.add(State("From"));
    //automata.add(State("Next"));
    //EXPECT_EQ(2, automata.size());
//
    //automata.add(State("From"));
    //automata.add(State("Next"));
    //automata.add(State("New"));
    //EXPECT_EQ(3, automata.size());
}

TEST_F(AutomataTest, add_multiple)
{   //automata.add(State("from", StateType::INIT), 'a', State("next", StateType::FINAL));
    //EXPECT_EQ(2, automata.size());
    //automata.add(State("next", StateType::NORMAL), 'a', State("from", StateType::NORMAL));
    //EXPECT_EQ(2, automata.size());
//
    //EXPECT_NO_THROW(State& from_state = automata.get(State("from")));
    //EXPECT_NO_THROW(State& next_state = automata.get(State("next")));
}

TEST_F(AutomataTest, get)
{   //automata.add(State("From"));
    //automata.add(State("Next"));
//
    //State& from_state = automata.get(State("From"));
    //State& next_state = automata.get(State("Next"));
//
    //EXPECT_TRUE(from_state.getName() == "From");
    //EXPECT_TRUE(next_state.getName() == "Next");
//
    //EXPECT_THROW(automata.get(State("UNDEFINED")), std::out_of_range);
}
