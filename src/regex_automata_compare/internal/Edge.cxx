////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/internal/Edge.hpp"
/******************************************************************************/
#include <stdexcept>
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {
namespace priv {

//--------------------------------------
Edge::Edge(StatePack& from, char alpha, StatePack& to)
: _from(from)
, _to(to)
, _alpha(alpha) {
}

//--------------------------------------
const StatePack&
Edge::getFromState() const {
    // \todo: complete this function.
    throw std::logic_error("code application logic here");
}

//--------------------------------------
const StatePack&
Edge::getToState() const {
    // \todo: complete this function.
    throw std::logic_error("code application logic here");
}

//--------------------------------------
char
Edge::getAlpha() const {
    // \todo: complete this function.
    throw std::logic_error("code application logic here");
}

} // namesapce priv
} // namespace regex_automata_compare::priv
