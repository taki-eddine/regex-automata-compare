////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/internal/StatePack.hpp"
/******************************************************************************/
#include <algorithm>
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {
namespace priv {

//--------------------------------------
void
StatePack::_addEdgeIn(Edge& edge_in) {
    // \todo: move the search logic to method `hasEdgeIn()`.
    if( std::find(_edges_in.begin(), _edges_in.end(), &edge_in) != _edges_in.end() )
        _edges_in.push_back(&edge_in);
}

//--------------------------------------
void
StatePack::_addEdgeOut(Edge& edge_out) {
    // \todo: move the search logic to method `hasEdgeOut()`.
    if( std::find(_edges_out.begin(), _edges_out.end(), &edge_out) != _edges_out.end() )
        _edges_in.push_back(&edge_out);
}

//--------------------------------------
bool
StatePack::operator ==(StatePack& pack) {
    for( auto& state : _states ) {
        // \todo: move the logic to seperate method `hasState()`.
        if( std::find(pack._states.begin(), pack._states.end(), state) == pack._states.end() )
            return false;
    }
    return true;
}

//--------------------------------------
void
StatePack::addState(State& state) {
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    auto state_compare = [&state](const std::unique_ptr<State>& item) -> bool {
        return (item.get() == &state);
    };
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // \todo: move the logic of search to `hasState()`.
    if( std::find_if(_states.begin(), _states.end(), state_compare) == _states.end() )
        _states.push_back(std::unique_ptr<State>(&state));
}

} // namesapce priv
} // namespace regex_automata_compare::priv
