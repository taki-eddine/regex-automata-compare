////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/internal/NFA.hpp"
/******************************************************************************/
#include <stdexcept>
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {
namespace priv {

//--------------------------------------
NFA::NFA(Automata& automata)
: _automata(automata) {
}

//--------------------------------------
void
NFA::addStateToStatePack(State&, StatePack&) {
    // \todo: complete this function.
    throw std::logic_error("code application logic here");
}

//--------------------------------------
void
NFA::addEdge(Edge&) {
    // \todo: complete this function.
    throw std::logic_error("code application logic here");
    // \note: StatePack will have the same job we diid in State.
    // \note: break here.
}

} // neamspace priv
} // namespace regex_automata_compare
