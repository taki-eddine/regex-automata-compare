////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/Transition.hpp"
/******************************************************************************/
#include "regex_automata_compare/State.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -* Public *- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//--------------------------------------
Transition::Transition(const std::shared_ptr<State>& start_state, const char alpha, const std::shared_ptr<State>& reach_state)
: _start_state(start_state)
, _alpha(alpha)
, _reach_state(reach_state) {
}

//--------------------------------------
const char&
Transition::getAlpha() const {
    return _alpha;
}

//--------------------------------------
const State&
Transition::getStartState() const {
    return *_start_state;
}

//--------------------------------------
const State&
Transition::getReachState() const {
    return *_reach_state;
}

//--------------------------------------
bool
Transition::operator==(const Transition& other) const {
    return (*_start_state == *other._start_state)
            && (_alpha == other._alpha)
            && (*_reach_state == *other._reach_state);
}

//--------------------------------------
std::string
Transition::toString() const {
    std::string to_string = std::string();

    to_string += _start_state->getLabel();
    to_string += ' ';
    to_string += static_cast<char>(_start_state->getType());
    to_string += ' ';
    to_string += _alpha;
    to_string += ' ';
    to_string += static_cast<char>(_reach_state->getType());
    to_string += ' ';
    to_string += _reach_state->getLabel();
    to_string += '\n';

    return to_string;
}

} // namespace regex_automata_compare
