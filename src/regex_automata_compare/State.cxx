////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/State.hpp"
/******************************************************************************/
#include <stdexcept>
#include <algorithm>
using namespace std::literals;
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                                -* Private *-                               //
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//--------------------------------------
const std::vector< std::shared_ptr<Transition> >&
State::_getEdgesOut() const {
    return _edges_out;
}

//--------------------------------------
const std::vector< std::shared_ptr<Transition> >&
State::_getEdgesIn() const {
    return _edges_in;
}

//--------------------------------------
void
State::_addEdgeOut(const std::shared_ptr<Transition>& transition) {
    _edges_out.push_back(transition);
}

//--------------------------------------
void
State::_removeEdgeOut(const Transition& transition) {
    _edges_out.erase( std::find_if(_edges_out.begin(), _edges_out.end(),
                                   [&transition](const std::shared_ptr<Transition>& item)
                                   -> bool {
                                       return (item.get() == &transition);
                                   }) );
}

//--------------------------------------
void
State::_addEdgeIn(const std::shared_ptr<Transition>& transition) {
    _edges_in.push_back(transition);
}

//--------------------------------------
void
State::_removeEdgeIn(const Transition& transition) {
    _edges_in.erase( std::find_if(_edges_in.begin(), _edges_in.end(),
                                  [&transition](const std::shared_ptr<Transition>& item)
                                  -> bool {
                                      return (item.get() == &transition);
                                  }) );
}

//--------------------------------------
bool
State::_addCompose(const std::shared_ptr<State>& state, bool regenerate_label) {
    // ingore adding self or adding already existed state.
    if( _hasComposer(state) )
        return false;

    _composers.push_back(state);

    // we check only for FINAL, init state will be only the one wich we explicity specified.
    if( state->_type == StateType::FINAL )
        _type = StateType::FINAL;

    // regenerate a name for the state.
    if( regenerate_label )
        _generateLabelFromComposers();

    return true;
}

//--------------------------------------
void
State::_addComposers(const std::list< std::shared_ptr<State> >& states) {
    bool is_there_new_state = false;

    for( const std::shared_ptr<State>& state : states )
        if( _addCompose(state, false) )
            is_there_new_state = true;

    // now we generate new label because `_addCompose()' didn't.
    if( is_there_new_state )
        _generateLabelFromComposers();
}

//--------------------------------------
void
State::_removeCompose(const State& state) {
    std::remove_if(_composers.begin(), _composers.end(),
                   [&state](const std::shared_ptr<State>& item)
                   -> bool {
                       return (item.get() == &state);
                   });
}

//--------------------------------------
bool
State::_hasComposer(const std::shared_ptr<State>& state) {
    return std::find_if(_composers.begin(), _composers.end(),
                        [&state](const std::shared_ptr<State>& item) -> bool {
                            return (item == state);
                        }) != _composers.end();
}

//--------------------------------------
void
State::_generateLabelFromComposers() {
    _label = _composers.front()->_label;
    auto i = _composers.begin();
    // skip the first one.
    for( i++; i != _composers.end(); i++ )
        _label += "," + (*i)->_label;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -* Public *- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//--------------------------------------
State::State() {
}

//--------------------------------------
State::State(const std::string& name, const StateType type)
: _label(name)
, _type(type) {
}

//--------------------------------------
bool
State::operator==(const State& other) const {
    return (_label == other._label);
}

//--------------------------------------
const std::string&
State::getLabel() const {
    return _label;
}

//--------------------------------------
void
State::setName(const std::string& name) {
    _label = name;
}

//--------------------------------------
StateType
State::getType() const {
    return _type;
}

//--------------------------------------
std::string
State::toString() const {
    return _label;
}

} // nanamespace regex_automata_compare
