////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/Automata.hpp"
/******************************************************************************/
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <map>
using namespace std::literals;
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -* Private *- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//--------------------------------------
void
Automata::_removeStateFromInitList(State& state) {
    _init_states.erase( std::find_if(_init_states.begin(), _init_states.end(),
                                     [&state](const std::shared_ptr<State>& item)
                                     -> bool {
                                         return item.get() == &state;
                                     }) );
}

//--------------------------------------
std::shared_ptr<State>
Automata::_findState(const std::string& label) {
    std::shared_ptr<State> state = nullptr;

    for( std::shared_ptr<State>& i : _states )
        if( i->getLabel() == label ) {
            state = i;
            break;
        }

    return state;
}

//--------------------------------------
auto
Automata::_findState(const State& state) {
    auto it = _states.begin();

    for( ; it != _states.end(); it++ )
        if( it->get() == &state )
            break;
    return it;
}

//--------------------------------------
std::shared_ptr<Transition>
Automata::_findTransition(State& start, char alpha, State& reach) {
    std::shared_ptr<Transition> transition  = nullptr;
    auto&                       transitions = start._getEdgesOut();

    for( const std::shared_ptr<Transition>& it : transitions )
        if( it->getAlpha() == alpha
            && it->_reach_state.get() == &reach ) {
            transition = it;
            break;
        }

    return transition;
}

//--------------------------------------
std::shared_ptr<Transition>
Automata::_findTransition(State& start, char alpha) {
    std::shared_ptr<Transition> transition  = nullptr;
    auto&                       transitions = start._getEdgesOut();

    // before startting, check that the automata is deterministic:
    if( !_is_deterministic )
        throw std::logic_error("Automta must be DFA before using this method.");

    for( const std::shared_ptr<Transition>& it : transitions )
        if( it->getAlpha() == alpha ) {
            transition = it;
            break;
        }

    return transition;
}

//--------------------------------------
auto
Automata::_findTransition(const Transition& transition) {
    auto it = _transitions.begin();

    for( ; it != _transitions.end(); it++ )
        if( it->get() == &transition )
            break;

    return it;
}

//--------------------------------------
void
Automata::_resetAlphabetsStates() {
    for( auto& alphabet : _alphabets )
        const_cast<std::pair<char, bool>&> (alphabet).second = false;
}

//--------------------------------------
std::pair<char, bool>&
Automata::_findAlphabet(const char c) {
    for( auto& alpha : _alphabets )
        if( alpha.first == c )
            return const_cast<std::pair<char, bool>&> (alpha);
    throw std::out_of_range("This must not be happen, but the alpha passed to '_findAlphabet()' can't be found.");
}

//--------------------------------------
bool
Automata::_isStateComplete(State& state) {
    // find the missing alphabet transition in.
    for( auto& alpha : _alphabets )
        if( !hasTransition(state, alpha.first) )
            return false;
    return true;
}

//--------------------------------------
bool
Automata::_isStateAmbiguous(State& state) {
    const std::vector< std::shared_ptr<Transition> >& transitions = state._getEdgesOut();

    _resetAlphabetsStates();
    for( const std::shared_ptr<Transition>& i : transitions ) {
        std::pair<char, bool>& alpha = _findAlphabet(i->getAlpha());
        if( alpha.second )
            return true;
        else alpha.second = true;
    }

    return false;
}

//--------------------------------------
/*void
Automata::_completeState(State&, State&){
}*/

//--------------------------------------
void
Automata::_completeStates() {
    // add a trap state before doing anything.
    State& trap = addState("T");

    // complete incompleted states.
    for( auto & i : _states ) {
        State& state = *i;

        // check each alphabet if exist in states transitions.
        for( auto & alpha : _alphabets )
            if(!hasTransition(state, alpha.first))
                addTransition(state, alpha.first, trap); // if any alphabets not found,

        // create new transition to the trap state.
    }

    // remove unreachable states because there is possiblity
    // that the trap state will be orphant in case there is any incomplete state.
    _removeUnreachableStates();  // \todo: add `_removeStateIfUnreachable()'.
}

//--------------------------------------
void
Automata::_disambiguateStates() {
    // \todo: complete this function.
    throw std::logic_error("code application logic here");
}

//--------------------------------------
void
Automata::_removeUnreachableStates() {
    // for each state:
    // check if state is unreachable
    // if true remove remove all links.
    _states.erase( std::remove_if(_states.begin(),
                                  _states.end(),
                                  [this](std::shared_ptr<State>& item)
                                  -> bool {
                                      if( item->_edges_in.size() == 0 ) {
                                          _removeStateLinks(*item);
                                          return true;
                                      }
                                      return false;
                                  }),
                  _states.end() );
}

//--------------------------------------
void
Automata::_removeStateLinks(State& state) {
    // foreach transition of the state
    for( std::shared_ptr<Transition>& transition : state._edges_out ) {
        // remove `to' targted by transition.
        transition->_reach_state->_removeEdgeIn(*transition);
        // remove the transition from Automata.
        _transitions.erase( _findTransition(*transition) );
    }
    // when finich clear the `targets transition' list.
    state._edges_out.clear();

    // foreach transition `targted by' to this state.
    for( std::shared_ptr<Transition>& transition : state._edges_in ) {
        // remove `from' targtest this transition.
        transition->_start_state->_removeEdgeOut(*transition);
        // remove the transition from Automata.
        _transitions.erase( _findTransition(*transition) );
    }
    // when finich clear the `targted by transitions' list.
    state._edges_in.clear();
}

//--------------------------------------
void
Automata::_closure(State& in_state) {
    // where are gonna chane the input state.
    // the list "state._composed" will grow when we are looking for reachable states.
    for( std::shared_ptr<State>& state : in_state._composers )
        for( std::shared_ptr<Transition>& edge : state->_edges_out )
            if( edge->getAlpha() == '&' )  // '&' == epsilon.
                in_state._addCompose(edge->_reach_state);
}

//--------------------------------------
std::shared_ptr<State>
Automata::_goto(State& in_state, char alpha) {
    std::shared_ptr<State> out = std::make_shared<State>();

    for(std::shared_ptr<State>& state : in_state._composers)
        for(std::shared_ptr<Transition>& edge : state->_edges_out)
            if(edge->getAlpha() == alpha)
                out->_addCompose(edge->_reach_state);
    // we must closure the new states.
    _closure(*out);
    return out;
}

//--------------------------------------
void
Automata::_assignLabesToStates() {
    int gen_name = 0;

    for( std::shared_ptr<State>& state : _states ) {
        // clear state's composers, we don't need them.
        state->_composers.clear();
        // assign to it a unique name.
        state->setName(std::to_string(gen_name));
        gen_name++;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -* Public *- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//--------------------------------------
Automata::Automata() {
}

//--------------------------------------
bool
Automata::isDeterministic() const {
    return _is_deterministic;
}

//--------------------------------------
State&
Automata::addState(const std::string& label, const StateType type) {
    std::shared_ptr<State> state = _findState(label);

    if( state == nullptr ) {
        state = std::make_shared<State>(label, type);
        _states.push_back(state);

        if( type == StateType::INIT )
            _init_states.push_back(state);
    }

    return *state;
}

//--------------------------------------
State&
Automata::addState(const std::shared_ptr<State>& p_state) {
    std::shared_ptr<State> s = _findState(p_state->_label);

    if( s == nullptr ) {
        // note: simply we take it, `_goto' did the whole job.
        s = p_state;
        _states.push_back(s);
    }

    return *s;
}

//--------------------------------------
bool
Automata::hasState(const std::string& label) {
    return _findState(label) != nullptr;
}

//--------------------------------------
State&
Automata::findState(const std::string& label) {
    std::shared_ptr<State> state = _findState(label);

    if( state == nullptr )
        throw std::out_of_range(label);

    return *state;
}

//-------------------------------------
void
Automata::removeState(State& state) {
    _removeStateLinks(state);

    // now remove the state.
    _states.erase( std::find_if(_states.begin(),
                                _states.end(),
                                [this, &state](const auto& item)
                                -> bool {
                                    if( item.get() == &state ) {
                                        // before removing we must check if this state is init.
                                        if( state.getType() == StateType::INIT )
                                            this->_removeStateFromInitList(state);
                                        return true;
                                    }

                                    return false;
                                }) );
}

//-------------------------------------
Transition&
Automata::addTransition(State& start, char alpha, State& reach) {
    // check if state already exist.
    std::shared_ptr<Transition> transition = _findTransition(start, alpha, reach);

    if( transition == nullptr ) {
        // this new transition
        transition = std::make_shared<Transition>(*_findState(start), alpha, *_findState(reach));
        _transitions.push_back(transition);

        start._addEdgeOut(transition);
        reach._addEdgeIn(transition);

        _alphabets.insert( {alpha, false} );
    }

    return *transition;
}

//--------------------------------------
bool
Automata::hasTransition(State& state, const char alpha) {
    for( const std::shared_ptr<Transition>& i : state._getEdgesOut() )
        if( i->getAlpha() == alpha )
            return true;
    return false;
}

//--------------------------------------
void
Automata::removeTransition(const Transition& transition) {
    // remove transition from the target.
    transition._start_state->_removeEdgeOut(transition);
    // remove transition from the target by.
    transition._reach_state->_removeEdgeIn(transition);
    // now remove it from transition vector.
    _transitions.erase(_findTransition(transition));
}

//--------------------------------------
void
Automata::determinize() {
    Automata dfa;
    // add the init states to DFA.
    dfa.addState("", StateType::INIT)._addComposers(_init_states);  // set init state (wich could composed).

    // loop until we don't have any new state.
    // the list will grow while by the call of "_goto()".
    for( std::shared_ptr<State>& i : dfa._states ) {
        State& state = *i;
        // closure the current state.
        _closure(state);  /// \note: we can optimize, only the first need it to closure, the rest will be closure in goto.

        // if not, foreach alpha we find the reachable state
        for( auto& alpha : _alphabets ) {
            // we must ignore '&'.
            if( alpha.first == '&' )
                continue;

            // add the new generated state,
            std::shared_ptr<State> reachable_state = _goto(state, alpha.first);

            // if `_goto()' return a empty state, that' means there isn't any reachable states.
            if( reachable_state->_label.size() == 0 )
                continue;

            // `addState()' will handle the checking job and return the propriete
            // object to work with it.
            State& destination = dfa.addState(reachable_state);

            // add a transition to our destination.
            dfa.addTransition(state, alpha.first, destination);
        }
    }

    // each state must have unique name, and clear it's composers.
    dfa._assignLabesToStates();
    // now move everthing to make the our automata the DFA.
    *this = std::move(dfa);

    _is_deterministic = true;
}

//--------------------------------------
void
Automata::minimize() {
    // TODO: complete this method make it usable only if the the automata is DFA.
    std::map< std::string,
              std::map<std::string, bool> > merges = { };

    // --------------------------------------
    // fill the keys with the apropriet States.
    for(auto i = _states.begin(); i != _states.end(); ++i)
        for(auto j = std::next(i, 1); j != _states.end(); ++j) {
            // if (First and Second are FINAL) or (First and Second both aren't FINAL)
            merges[(*i)->_label][(*j)->_label] = !(((*i)->_type == StateType::FINAL) ^ ((*j)->_type == StateType::FINAL));
        }

    // --------------------------------------
    // create table of merges:
    bool merges_has_changed = false;

    do {
        merges_has_changed = false;

        // for: cycle trough the merges
        for (auto i = merges.begin(); i != merges.end(); ++i) {
            for (auto j = i->second.begin(); j != i->second.end(); ++j) {
                // check if is true to test.
                if (j->second == true) {
                    // for each First State (First key) and Second State (Second Key)
                    State& first  = *_findState(i->first);
                    State& second = *_findState(j->first);

                    // foreach alpahat:
                    for (std::pair<char, bool> alpha : _alphabets) {
                        // get the transtions for each State `getTransition(<First|Second>. alpha)'.
                        std::shared_ptr<Transition> first_transition  = _findTransition(first, alpha.first);
                        std::shared_ptr<Transition> second_transition = _findTransition(second, alpha.first);

                        // if one of them doesn't have a transition then the states can't be merged.
                        if ((first_transition == nullptr) ^ (second_transition == nullptr)) {
                            // and mark that the matrix has been changed.
                            j->second = false;
                            merges_has_changed = true;
                            break;
                        }

                        // both of them are null (the second is implicite because we XOR in previouse).
                        if (first_transition == nullptr)
                            continue;

                        // if the transtion point to the same state, then these are mergeable, so continue to next.
                        if (first_transition->_reach_state->_label == second_transition->_reach_state->_label)
                            continue;

                        // else get the reached states of both of them:
                        State& first_reach  = *(first_transition->_reach_state);
                        State& second_reach = *(second_transition->_reach_state);

                        // if the reached states are not mergeable
                        if (merges[first_reach._label][second_reach._label] == false) {
                            // mark our First and Second states that they are not mergeable and mark that the matrix has changed.
                            j->second = false;
                            merges_has_changed = true;
                        }
                    }
                }
            }
        }
    } while( merges_has_changed );

    // --------------------------------------
    // collect the status wich they should be merged.
    // TODO: use `forward_list`.
    std::list< std::set<std::string> > unique_merges = { };

    for(auto& i : merges) {
        for(auto& j : i.second) {
            // search for mergeable couples.
            if( !j.second )
                continue;

            // cyle through the `unique_merges` to find if they already exist:
            bool is_new_merge = true;

            for(auto& k : unique_merges) {
                // check the first `state`
                auto first  = k.find(i.first);
                auto second = k.find(j.first);

                // both exist
                if( (first != k.end()) && (second != k.end()) ) {
                    is_new_merge = false;
                    break;
                }

                // first exist, add second with him
                if( first != k.end() ) {
                    is_new_merge = false;
                    k.insert(*second);
                    break;
                }

                // second exist, add first with him
                if( second != k.end() ) {
                    is_new_merge = false;
                    k.insert(*first);
                    break;
                }
            }

            // we looped all merges and none one of them exists, add new merge
            if( is_new_merge )
                unique_merges.push_front( std::set<std::string>{i.first, j.first} );
        }
    }

    // --------------------------------------
    // create list of states to merge
    std::forward_list< std::forward_list<std::reference_wrapper<State>> > states_to_merge { };

    // foreach label merge group
    for(std::set<std::string>& group_labels : unique_merges) {
        // insert new pointer merge group
        states_to_merge.push_front( std::forward_list< std::reference_wrapper<State> >{ } );
        std::forward_list< std::reference_wrapper<State> >& states = states_to_merge.front();

        // foreach label
        for(const std::string& label : group_labels) {
            // insert the object
            states.push_front(*_findState(label));
        }
    }

    for(std::forward_list<std::reference_wrapper<State>>& group : states_to_merge)
        mergeStates(group);

    // reassign new labels.
    _assignLabesToStates();
}

//--------------------------------------
void
Automata::mergeStates(const std::forward_list<std::reference_wrapper<State>>& states_to_merge) {
    // TODO: find a better way, maybe we can save the `shared_ptr' in each state so we can avoid the search.
    // we must create new list wich has `shared_ptr`.
    std::list< std::shared_ptr<State> > composers { };

    for(auto& state : states_to_merge)
        composers.push_front(_findState(state.get()._label));

    // create a empty new_state.
    State& new_state = addState("");
    // insert state to merge into new_state_composers.
    new_state._addComposers(composers);

    // if one the state is of type INIT, then the new state must be of type INIT (PS: FINAL is test in `addComposers()`).
    for(auto& state : new_state._composers)
        if(state->_type == StateType::INIT)
            new_state._type = StateType::INIT;

    // --------------------------------------
    for(auto& composer : new_state._composers) {
        // change the in edge to point to the new transition.
        for(auto& edge_in : composer->_edges_in) {
            // we are merging so any internal transition must be ignore.
            bool is_start_compose = false;

            for(auto& i : new_state._composers)
                if(edge_in->_start_state->_label == i->_label) {
                    is_start_compose = true;
                    break;
                }

            if(is_start_compose)
                continue;

            // NOTE: maybe we can optimse, after create transition remove the original transition.
            // add transition to this state if if there isn't already transition with the sam aplpha to avoid ambigious automata.
            if(_findTransition(*(edge_in->_start_state), edge_in->_alpha, new_state) == nullptr)
                addTransition(*(edge_in->_start_state), edge_in->_alpha, new_state);
        }

        // add trantion to the state pointed by the composers.
        for(auto& edge_out : composer->_edges_out) {
            // if edgeOut lead to one of the composers so ignore it except to it self.
            bool is_reach_compose = false;

            for(auto& i : new_state._composers)
                if(composer->_label == i->_label) // ignore because it obvious that this composer exist.
                    continue;
                else if(edge_out->_reach_state->_label == i->_label) {
                    is_reach_compose = true;
                    break;
                }

            // if it reach one of the compose that's mean surely there's a state has a reflexiv transition.
            if(is_reach_compose)
                continue;

            // if it's reflixiv edge.
            if(edge_out->_reach_state->_label == composer->_label)
                addTransition(new_state, edge_out->_alpha, new_state);
            else addTransition(new_state, edge_out->_alpha, *(edge_out->_reach_state));
        }
    }

    // foreach state of the new_state.composers
    for(auto& state : new_state._composers)
        removeState(*state);

    new_state._composers.clear();
}

//--------------------------------------
void
Automata::load(const std::string& filename) {
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    auto tokenize = [] (std::string & str) {
        std::vector<std::string> tokens;
        std::istringstream  line       = std::istringstream(str);
        char                buffer[20] = {'\0'};

        while( line.getline(buffer, 20, ' ') )
            tokens.push_back(std::string(buffer));

        return tokens;
    };
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    char          freader_buffer[8192] = {'\0'};
    std::ifstream freader;

    freader.rdbuf()->pubsetbuf(freader_buffer, sizeof(freader_buffer));
    freader.open(filename);
    if( !freader.is_open() )
        throw std::ios::failure("IO exception: error opening '"s + filename + "'");

    // Read the file
    std::string line = std::string();
    while( std::getline(freader, line) ) {
        if( line.empty() )
            continue;

        std::vector<std::string> tokens = tokenize(line);

        if( tokens.size() != 2 && tokens.size() != 5 )
            throw std::runtime_error("File doesn't follow the syntax");

        // syntax: from type
        std::string& from_label = tokens[0];
        char         from_type  = tokens[1][0];
        State&       from       = addState(from_label, charToStateType(from_type));

        if( tokens.size() == 5 ) {
            // syntax: from type alpha type to
            char         alpha    = tokens[2][0];
            std::string& to_label = tokens[4];
            char         to_type  = tokens[3][0];
            State&       to       = addState(to_label, charToStateType(to_type));

            addTransition(from, alpha, to);
        }
    }
}

//--------------------------------------
void
Automata::save(const std::string& filename) {
    std::ofstream fwriter;

    fwriter.open(filename);
    if( !fwriter.is_open() )
        throw std::ios::failure("IO exception: error opening '"s + filename + "'");

    fwriter << toString();
    fwriter.close();
}

//--------------------------------------
std::size_t
Automata::size() const {
    return _states.size();
}

//--------------------------------------
std::string
Automata::toString() const {
    std::string to_string = std::string();

    for( auto& i : _transitions )
        to_string += i->toString();

    return to_string;
}

} // namespace regex_automata_compare
