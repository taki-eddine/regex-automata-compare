////////////////////////////////////////////////////////////////////////////////
#include "regex_automata_compare/StateType.hpp"
/******************************************************************************/
#include <string>
#include <stdexcept>
using namespace std::literals;
////////////////////////////////////////////////////////////////////////////////
namespace regex_automata_compare {

//--------------------------------------
StateType charToStateType(const char char_state_type) {
    switch( char_state_type ) {
        case '+':
        case '-':
        case '*':
            return static_cast<StateType>(char_state_type);
        default:
            throw std::invalid_argument("Invalid argument: 'charToStateType()' "
                                        "accept only {+, -, *}: passed '"s + char_state_type + "'.");
    }
}

} // namespace regex_automata_compare
