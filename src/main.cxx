////////////////////////////////////////////////////////////////////////////////
#include <cstdio>
#include "regex_automata_compare/Automata.hpp"
using namespace regex_automata_compare;
////////////////////////////////////////////////////////////////////////////////

//--------------------------------------
int main(int, char**) {
    Automata automata = Automata();

    automata.load("automata.txt");
    std::printf("Automata: \n%s \n", automata.toString().c_str());
    automata.determinize();
    automata.save("gen-automata.txt");
    automata.minimize();
    automata.save("opt-automata.txt");
    return 0;
}
